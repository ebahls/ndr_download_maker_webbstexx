﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

using HypWcfExtension.Contract;
using HypWcfExtension.DataContract;
using HypWcfExtension.Infrastructure.Logging;
using HypWcfExtension.ServiceFacade;
using HypWcfExtension.ConstructionLib;
using System.Web.Script.Serialization;

using StructureMap;
using HypWcfExtension.Models;
using NDRDownloadMaker;
using log4net;
using System.Threading.Tasks;

namespace HypWcfExtension.Service
{

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class HypExtensionService : IHypExtensionService
    {
        const int RCHypExtensionServiceExceptionCaught = 20000;
        const string CBE_TOOL = "Eplan oder Microstation Packet";
        const string CBE_SELECTEDFILES = "Nur gewählte Dateien Downloaden";
        const string CBE_RECURSIV = "Gesamten Ordner Downloaden";


        //*************************************************************************
        //* InitExtension
        //*************************************************************************
        public InitExtensionResponse InitExtension(DefaultRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "InitExtension (request={0})",
                                            request.MakeString());

            InitExtensionResponse response = new InitExtensionResponse();
            response.ErrorID = 0;
            response.CallbackFunctions = new List<DCCallbackFunction>();

            //IET.AddStampCallback(response.CallbackFunctions, Methods.VERIFYINDEXDATA, "", "");
            //IET.AddStampCallback(response.CallbackFunctions, Methods.VERIFYQUERYDATA, "", "");
            //IET.AddStampCallback(response.CallbackFunctions, Methods.QUERYSTAMPLOAD, "", "");
            //IET.AddStampCallback(response.CallbackFunctions, Methods.INDEXSTAMPLOAD, "", "");

            //IET.AddCallback(response.CallbackFunctions, Methods.MAINMENULOAD, "");
            //IET.AddCallback(response.CallbackFunctions, Methods.DOCUMENTMENULOAD, "");
            IET.AddCallback(response.CallbackFunctions, Methods.HITLISTMENULOAD, "");
            //IET.AddCallback(response.CallbackFunctions, Methods.LOGOUT, "");
                      
            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "InitExtension (ErrorID={0}, request={1}) user {2}",
                                            response.ErrorID, request.MakeString(), request.UserToken);
           

            return response;
        }

        //*************************************************************************
        //* VerifyIndexData
        //*************************************************************************
        public VerifyDataResponse VerifyIndexData(VerifyIndexDataRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "VerifyIndexData (request={0})",
                                            request.MakeString());


            VerifyDataResponse response = new VerifyDataResponse();

            response.VerifyOK = true;
            response.Message = "";
            response.ErrorID = 0; // soll diese ErrorID noch eine Bedeutung bekommen?

            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "VerifyIndexData (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());
            return response;
        }

        //*************************************************************************
        //* VerifyQueryData
        //*************************************************************************
        public VerifyDataResponse VerifyQueryData(VerifyQueryDataRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "VerifyQueryData (request={0})",
                                            request.MakeString());

            VerifyDataResponse response = new VerifyDataResponse();

            response.Message = "";
            response.VerifyOK = true;
            response.ErrorID = 0; // soll diese ErrorID noch eine Bedeutung bekommen?

            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "VerifyQueryData (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());
            return response;
        }

        //*************************************************************************
        //* QueryStampLoad
        //*************************************************************************
        public LoadResponse QueryStampLoad(QueryStampLoadRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "QueryStampLoad (request={0})",
                                            request.MakeString());

            LoadResponse response = new LoadResponse(); // create response class

            List<DCEventMethods> EventMethods = new List<DCEventMethods>();
            string HtmlContent = "";

            response.HtmlAddtional = HtmlContent;
            response.EventMethods = EventMethods;
            response.FieldValues = new List<DCFieldValue>(); // hier sollte ein Null-Pointer verkraftet werden!

            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "QueryStampLoad (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());

            return response;
        }

        //*************************************************************************
        //* QueryStampAjax
        //*************************************************************************
        public AjaxResponse QueryStampAjax(QueryStampAjaxRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "QueryStampAjax (request={0})",
                                            request.MakeString());

            AjaxResponse response = new AjaxResponse();

            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "QueryStampAjax (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());

            return response;
        }

        //*************************************************************************
        //* IndexStampLoad
        //*************************************************************************
        public LoadResponse IndexStampLoad(IndexStampLoadRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "IndexStampLoad (request={0})",
                                            request.MakeString());

            LoadResponse response = new LoadResponse(); // create response class

            List<DCEventMethods> EventMethods = new List<DCEventMethods>();
            string HtmlContent = "";

            response.HtmlAddtional = HtmlContent;
            response.EventMethods = EventMethods;
            response.FieldValues = new List<DCFieldValue>(); // hier sollte ein Null-Pointer verkraftet werden!


            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "IndexStampLoad (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());

            return response;
        }

        //*************************************************************************
        //* IndexStampAjax
        //*************************************************************************
        public AjaxResponse IndexStampAjax(IndexStampAjaxRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "IndexStampAjax (request={0})",
                                            request.MakeString());

            AjaxResponse response = new AjaxResponse();

            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "IndexStampAjax (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());

            return response;
        }

        //*************************************************************************
        //* MainMenuLoad
        //*************************************************************************
        public MenuLoadResponse MainMenuLoad(MainMenuLoadRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "MainMenuLoad (request={0})",
                                            request.MakeString());

            List<DCMenuEntry> MenuEntries = new List<DCMenuEntry>();

            MenuLoadResponse response = new MenuLoadResponse();
            response.MenuEntries = MenuEntries;

            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "MainMenuLoad (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());

            return response;
        }

        //*************************************************************************
        //* MainMenuAjax
        //*************************************************************************
        public AjaxResponse MainMenuAjax(AjaxRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "MainMenuAjax (request={0})",
                                            request.MakeString());

            AjaxResponse response = new AjaxResponse();

            response.Message = "";
            response.ErrorID = 0;

            response.jsonResultString = "";

            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "MainMenuAjax (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());

            return response;
        }

        //*************************************************************************
        //* DocumentMenuLoad
        //*************************************************************************
        public MenuLoadResponse DocumentMenuLoad(DocumentMenuLoadRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "DocumentMenuLoad (request={0})",
                                            request.MakeString());

            List<DCMenuEntry> MenuEntries = new List<DCMenuEntry>();

            MenuLoadResponse response = new MenuLoadResponse();

            response.MenuEntries = MenuEntries;

            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "DocumentMenuLoad (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());

            return response;
        }

        //*************************************************************************
        //* DocumentMenuAjax
        //*************************************************************************
        public AjaxResponse DocumentMenuAjax(DocumentAjaxRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "DocumentMenuAjax (request={0})",
                                            request.MakeString());

            AjaxResponse response = new AjaxResponse();

            response.Message = "";
            response.ErrorID = 0;


            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "DocumentMenuAjax (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());

            return response;
        }

        //*************************************************************************
        //* HitlistMenuLoad
        //*************************************************************************
        public MenuLoadResponse HitlistMenuLoad(HitlistMenuLoadRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "HitlistMenuLoad (request={0})",



                                            request.MakeString());

            List<DCMenuEntry> MenuEntries = new List<DCMenuEntry>();

            MenuLoadResponse response = new MenuLoadResponse();
            response.MenuEntries = MenuEntries;

            HSC.CreateMenuCallback("HITLIST_I", "I", "Dokumente Anfordern", HSC.MENU_USE_CUSTOM_DIALOG, MenuEntries);
            
            response.MenuEntries = MenuEntries;
            response.menuLabel = "Download";


            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "HitlistMenuLoad (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());

            return response;
        }

        //*************************************************************************
        //* HitlistMenuAjax
        //*************************************************************************
        public AjaxResponse HitlistMenuAjax(HitlistAjaxRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "HitlistMenuAjax (request={0})",
                                            request.MakeString());
           
            AjaxResponse response = new AjaxResponse();

            response.ErrorID = 0;

            ILog log = LogManager.GetLogger("mylogger");
            List<string> dataFields = new List<string>();
            List<string> comboList = new List<string>();

            string dialogString = "";
            string mailaddr = "-";
            if (request.ajaxInfoTag == HSC.MENU_USE_CUSTOM_DIALOG) // Dialog-Definitions-Modus?
            {

                try
                {
                    Config conf = HelperFacade.loadConfig(log);
                    DownloadMaker dm = new DownloadMaker(conf,request.UserToken, log);
                    mailaddr = dm.mailAdresse;
                    foreach (DCHitlistEntry entr in request.DCHitlistEntries)
                    {
                        dm.addDocument((int)entr.DdbID, (int)entr.DocID);

                    }
                    dm.processRequestPrep(true);
                    List<Tool> tools = dm.getInvolvedTools();

                    if (tools.Count>0)
                    {
                        comboList.Add(CBE_TOOL);
                        comboList.Add(CBE_SELECTEDFILES);
                        dialogString += HSC.DialogCombo("Download als:", "downloadAS", comboList, dataFields);
                    }
                    else if (request.DCHitlistEntries.Count==1)
                    {
                        comboList.Add(CBE_RECURSIV);
                        comboList.Add(CBE_SELECTEDFILES);
                        dialogString += HSC.DialogCombo("Download als:", "downloadAS", comboList, dataFields);
                    }
                    else
                    {
                        comboList.Add(CBE_SELECTEDFILES);
                        dialogString += HSC.DialogCombo("Download als:", "downloadAS", comboList, dataFields);
                    }
                    

                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }

              


                


                dialogString += HSC.HiddenDialogData("hitlist", request.jsonData, dataFields);
                dialogString += HSC.HiddenDialogData("callmethod", request.method, dataFields);

                dialogString += HSC.DialogField("Version vom", "version", DateTime.Today.ToString("dd.MM.yyyy"), dataFields);
                dialogString += HSC.DialogField("Email an", "mail", mailaddr, dataFields);


                dialogString += HSC.FinalizeDialog(dataFields);
               
                
               
                response.jsonResultString = dialogString;
            }
               
            
            else
            {

              
                try
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    Dictionary<string, string> json = serializer.Deserialize<Dictionary<string, string>>(request.jsonData);
                    string mailadr = ""; json.TryGetValue("mail", out mailaddr);
                    string dlType = ""; json.TryGetValue("downloadAS", out dlType);
                    response.Message = "Mail mit Download Link wird in kürze an:" + mailaddr;


                    Config conf = HelperFacade.loadConfig(log);
                    DownloadMaker dm = new DownloadMaker(conf,request.UserToken, log);

                    if (log.IsDebugEnabled)
                    {
                        foreach (KeyValuePair<string, string> kv in json.AsEnumerable())
                        {
                            log.Debug("request key :" + kv.Key + " = " + kv.Value);
                        }
                    }

                    // tool downloads
                    List<string> urls = new List<string>();
                    if (dlType.CompareTo(CBE_TOOL)==0)
                    {
                        log.Info(dm.user + " starts tool download");
                        try
                        {
                            foreach (DCHitlistEntry entr in request.DCHitlistEntries)
                            {
                                dm.addDocument((int)entr.DdbID, (int)entr.DocID);

                            }
                            Task.Factory.StartNew(() => dm.zipAndShipTool(mailaddr, true));
                            
                            /*
                            dm.processRequestPrep(true);
                            List<Tool> tools = dm.getInvolvedTools();
                            foreach(Tool t in tools)
                            {
                                dm.collectAllFromStamp(t.stamp, t.field, t.objectID);

                                DateTime date = DateTime.Now;
                                string fileName = date.ToString("HH-mm-ss_dd_MM_yyyy") + "_" + t.objectID + "_" + t.name + ".zip";
                                string url = dm.toZipFile(dm.getRoot(), fileName);
                                
                                log.Info(url);
                                urls.Add(url);
                            }
                            //HelperFacade.sendMail(mailaddr, conf, urls, log);
                            */
                        }
                        catch(Exception ex)
                        {
                            log.Error(ex);
                        }


                    }else if (dlType.CompareTo(CBE_RECURSIV) == 0)
                    {
                        log.Info(dm.user + " start recusive download");
                        try
                        {

                             DCHitlistEntry entr = request.DCHitlistEntries[0];
                            
                             dm.addDocument((int)entr.DdbID, (int)entr.DocID);
                            Task.Factory.StartNew(() => dm.zipAndShipRecursive(mailaddr));
                            /*
                             dm.processRequestPrep(false);

                            dm.singleReqFolder(dm.content[0]);

                           

                                DateTime date = DateTime.Now;
                                string fileName = date.ToString("HH-mm-ss_dd_MM_yyyy") + "_Verzeichnis.zip";
                                string url = dm.toZipFile(dm.getRoot(), fileName);
                                log.Info(url);
                                urls.Add(url);
                            
                            HelperFacade.sendMail(mailaddr, conf, urls, log);
                            */
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex);
                        }
                    }
                    else if (dlType.CompareTo(CBE_SELECTEDFILES) == 0)
                    {
                        log.Info(dm.user + " start single file download");
                        try
                        {
                           

                            foreach (DCHitlistEntry entr in request.DCHitlistEntries)
                            {
                                dm.addDocument((int)entr.DdbID, (int)entr.DocID);

                            }

                            Task.Factory.StartNew(() => dm.zipAndShipSelected(mailaddr));

                            /*
                            dm.processRequestPrep(false);

                            DateTime date = DateTime.Now;
                            string fileName = date.ToString("HH-mm-ss_dd_MM_yyyy") + "_Verzeichnis.zip";
                            string url = dm.toZipFile(dm.getRoot(), fileName);
                            log.Info(url);
                            urls.Add(url);

                            HelperFacade.sendMail(mailaddr, conf, urls, log);
                            */
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex);
                        }
                    }


          

                }
                catch (Exception ex)
                {
                    log.Error(ex);
                  
                }

                

               
                
            }


          

            return response;
        }

        //*************************************************************************
        //* LogoutEvent
        //*************************************************************************
        public DefaultResponse LogoutEvent(DefaultRequest request)
        {
            LoggingFactory.GetLogger().Log(LoggingTypes.LogType.INFO,
                                            "LogoutEvent (request={0})",
                                            request.MakeString());

            DefaultResponse response = new DefaultResponse();

            LoggingFactory.GetLogger().Log(response.ErrorID == 0 ? LoggingTypes.LogType.INFO : LoggingTypes.LogType.ERROR,
                                            "LogoutEvent (request={0}, response={1})",
                                            request.MakeString(), response.MakeString());

            return response;
        }
    }
}
